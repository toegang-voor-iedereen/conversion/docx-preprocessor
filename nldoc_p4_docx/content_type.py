from enum import Enum


class ContentType(Enum):
    DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    DOCX_P4 = (
        "application/"
        "vnd-nldoc.preprocessed+openxmlformats-officedocument.wordprocessingml.document"
    )
