import abc
import typing

from .dto import ConversionMessage, ConversionTypeMapping


class Converter(abc.ABC):
    @abc.abstractmethod
    def convert(self, message: ConversionMessage) -> ConversionMessage:
        pass

    @abc.abstractmethod
    def supported_conversions(self) -> typing.List[ConversionTypeMapping]:
        pass

    def supports_conversion(self, mapping: ConversionTypeMapping) -> bool:
        return any(
            (
                mapping.to_content_type == supported.to_content_type
                and mapping.from_content_type == supported.from_content_type
            )
            for supported in self.supported_conversions()
        )

    def supports_message(self, message: ConversionMessage) -> bool:
        return self.supports_conversion(message.conversion_stack[0])
