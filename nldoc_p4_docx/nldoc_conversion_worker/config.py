import pydantic


class AmqpConnectionConfig(pydantic.BaseModel):
    host: str = "localhost"
    port: int = 5672
    vhost: str = "/"
    username: str = "guest"
    password: str = "guest"


class ExchangeConfig(pydantic.BaseModel):
    conversion: str = "exchange.conversion"
    dlx: str = "exchange.conversion.dlx"
    outbound: str = "exchange.conversion.outbound"
    unrouted: str = "exchange.unrouted"


class QueueConfig(pydantic.BaseModel):
    name: str = "p4"


class S3Config(pydantic.BaseModel):
    endpoint_url: str = "http://localhost:9000"
    bucket: str = "bucket"
    access_key: str = "minioadmin"
    secret_key: str = "minioadmin"
    region: str = "us-east-1"
