import base64
import typing
import annotated_types
import pydantic

NonEmptyStr = typing.Annotated[str, annotated_types.MinLen(1)]


def alias(name: str) -> str:
    components = name.split("_")

    return components[0] + "".join(x.title() for x in components[1:])


class BaseDto(pydantic.BaseModel):
    model_config = pydantic.ConfigDict(extra="forbid", populate_by_name=True, alias_generator=alias)


class Asset(BaseDto):
    filename: NonEmptyStr
    content_location: NonEmptyStr = pydantic.Field()


class ConversionData(BaseDto):
    assets: typing.List[Asset] = []
    content_location: NonEmptyStr = pydantic.Field()


class ConversionTypeMapping(BaseDto):
    # using alias as `from` is a reserved keyword in Python.
    from_content_type: NonEmptyStr = pydantic.Field(alias="from")
    to_content_type: NonEmptyStr = pydantic.Field(alias="to")

    def routing_key(self) -> str:
        return base64.b64encode(
            f"from:{self.from_content_type};to:{self.to_content_type}".encode()
        ).decode()


class ConversionMessage(BaseDto):
    identifier: NonEmptyStr
    mapping: ConversionTypeMapping
    data: ConversionData
    conversion_stack: typing.List[ConversionTypeMapping] = pydantic.Field(alias="conversionStack")
    meta: typing.Optional[typing.Dict[str, str]] = {}
