class UnsupportedConversionException(ValueError):
    def __init__(self, from_content_type: str, to_content_type: str):
        self.from_content_type = from_content_type
        self.to_content_type = to_content_type

        super().__init__(
            f"Unsupported conversion from '{from_content_type}' to '{to_content_type}'."
        )
