import json

import pika

from .config import QueueConfig, ExchangeConfig
from .converter import Converter
from .dto import ConversionMessage


class ConversionWorker:
    MAX_FAILURES = 3
    HEADER_FAILURE_COUNT = "x-failure-count"
    HEADER_MAX_FAILURES = "x-max-failures"
    HEADER_PREFIX_FAILURE_REASON = "x-failure-reason"

    def __init__(
        self,
        converter: Converter,
        connection: pika.BlockingConnection,
        queue: QueueConfig,
        exchange: ExchangeConfig,
    ):
        self.converter = converter
        self.connection = connection
        self.queue = queue
        self.exchange = exchange

    def setup(self):
        with self.connection.channel() as channel:
            channel.exchange_declare(
                exchange=self.exchange.unrouted, exchange_type="fanout", durable=True
            )

            channel.exchange_declare(
                exchange=self.exchange.outbound, exchange_type="fanout", durable=True
            )

            channel.exchange_declare(
                exchange=self.exchange.conversion,
                exchange_type="direct",
                durable=True,
                arguments={"alternate-exchange": self.exchange.unrouted},
            )

            channel.queue_declare(
                queue=self.queue.name,
                durable=True,
                auto_delete=False,
                arguments={"x-dead-letter-exchange": self.exchange.dlx},
            )

            for mapping in self.converter.supported_conversions():
                channel.queue_bind(
                    exchange=self.exchange.conversion,
                    queue=self.queue.name,
                    routing_key=mapping.routing_key(),
                )

    def handle(
        self,
        channel,
        msg_method: pika.spec.Basic.Deliver,
        properties: pika.spec.BasicProperties,
        body: bytes,
    ) -> None:
        failure_count = int(properties.headers.get(self.HEADER_FAILURE_COUNT, 0))

        print(
            f"Handling message {properties.message_id} with headers {properties.headers} "
            f"failure_count={failure_count}"
        )

        if failure_count >= self.MAX_FAILURES:
            print(
                f"Handling message {properties.message_id} with headers {properties.headers} "
                "failure count too high, nacking "
                f"failure_count={failure_count} MAX_FAILURES={self.MAX_FAILURES} "
            )

            channel.basic_nack(delivery_tag=msg_method.delivery_tag, requeue=False)
            return

        print(f"Deserializing message {properties.message_id} ")

        message = ConversionMessage(**json.loads(body))

        try:
            print(f"Converting message {properties.message_id} ")
            converted_message = self.converter.convert(message)
            print(f"Converted message {properties.message_id} ")
        except Exception as e:
            # @TODO: report to Sentry?

            print(f"Received exception on message {properties.message_id}: {e}")

            channel.basic_publish(
                exchange=self.exchange.conversion,
                routing_key=msg_method.routing_key,
                body=body,
                properties=pika.spec.BasicProperties(
                    delivery_mode=pika.DeliveryMode.Persistent,
                    content_type="application/json",
                    headers={
                        **properties.headers,
                        self.HEADER_FAILURE_COUNT: failure_count + 1,
                        self.HEADER_MAX_FAILURES: self.MAX_FAILURES,
                        f"{self.HEADER_PREFIX_FAILURE_REASON}-{failure_count + 1}": str(e),
                    },
                ),
            )

            channel.basic_ack(delivery_tag=msg_method.delivery_tag)

            print(f"Ack'd and published {properties.message_id} after exception: {e}")

            raise

        payload = json.dumps(converted_message.model_dump(by_alias=True)).encode("utf-8")

        print(f"Republishing converted variant of {properties.message_id}")

        if len(converted_message.conversion_stack) > 0:
            next_conversion = converted_message.conversion_stack.pop(0)
            channel.basic_publish(
                self.exchange.conversion,
                next_conversion.routing_key(),
                payload,
                properties=pika.spec.BasicProperties(
                    delivery_mode=pika.DeliveryMode.Persistent,
                    content_type="application/json",
                ),
            )

            print(f"Published to exchange converted variant of {properties.message_id}")
        else:
            channel.basic_publish(
                self.exchange.outbound,
                "",
                payload,
                properties=pika.spec.BasicProperties(
                    delivery_mode=pika.DeliveryMode.Persistent,
                    content_type="application/json",
                ),
            )

            print(f"Published to outbound converted variant of {properties.message_id}")

        channel.basic_ack(delivery_tag=msg_method.delivery_tag)

        print(f"Ack'd {properties.message_id}")

    def consume(self) -> None:
        with self.connection.channel() as channel:
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(self.queue.name, self.handle)

            try:
                channel.start_consuming()
            except KeyboardInterrupt:
                channel.stop_consuming()
                self.connection.close()
