import boto3
import pika

from .config import AmqpConnectionConfig, S3Config


def establish_amqp_connection(amqp: AmqpConnectionConfig) -> pika.BlockingConnection:
    return pika.BlockingConnection(
        pika.ConnectionParameters(
            host=amqp.host,
            port=amqp.port,
            virtual_host=amqp.vhost,
            credentials=pika.PlainCredentials(
                username=amqp.username,
                password=amqp.password,
            ),
        )
    )


def establish_s3_connection(s3: S3Config):
    return boto3.client(
        "s3",
        endpoint_url=s3.endpoint_url,
        aws_access_key_id=s3.access_key,
        aws_secret_access_key=s3.secret_key,
        region_name=s3.region,
    )
