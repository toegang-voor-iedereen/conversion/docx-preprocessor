import zipfile

import lxml.etree

from .docx.document import Document
from .docx.style import Styles
from .docx.theme import Theme
from .docx_document import DocxDocument


def xml_to_string(xml: lxml.etree.Element) -> bytes:
    info = xml.getroottree().docinfo

    attributes = f'version="1.0" encoding="{info.encoding}"'

    if info.standalone:
        attributes += ' standalone="yes"'

    return str.encode(f"<?xml {attributes}?>\r\n") + lxml.etree.tostring(
        xml, xml_declaration=False, encoding=info.encoding
    )


class DocxIO:
    PATH_DOCUMENT = "word/document.xml"
    PATH_STYLES = "word/styles.xml"
    PATH_THEME = "word/theme/theme1.xml"

    def __init__(self, docx_path: str):
        self.docx_path = docx_path

    def open(self) -> zipfile.ZipFile:
        return zipfile.ZipFile(self.docx_path, "r")

    def read(self) -> DocxDocument:
        with self.open() as file:
            with file.open(self.PATH_DOCUMENT) as document_file:
                document = Document(lxml.etree.parse(document_file).getroot())

            with file.open(self.PATH_STYLES) as styles_file:
                styles = Styles(lxml.etree.parse(styles_file).getroot())

            with file.open(self.PATH_THEME) as theme_file:
                theme = Theme(lxml.etree.parse(theme_file).getroot())

        return DocxDocument(document=document, styles=styles, theme=theme)

    def merge(self, destination_path: str, docx: DocxDocument) -> None:
        with zipfile.ZipFile(destination_path, "w") as output_zip:
            with output_zip.open(self.PATH_DOCUMENT, "w") as document_file:
                document_file.write(xml_to_string(docx.document.xml))

            with output_zip.open(self.PATH_STYLES, "w") as styles_file:
                styles_file.write(xml_to_string(docx.styles.xml))

            with output_zip.open(self.PATH_THEME, "w") as theme_file:
                theme_file.write(xml_to_string(docx.theme.xml))

            with self.open() as file:
                for name in file.namelist():
                    if name in [self.PATH_DOCUMENT, self.PATH_STYLES, self.PATH_THEME]:
                        continue

                    with file.open(name) as input_file:
                        output_zip.writestr(name, input_file.read())
