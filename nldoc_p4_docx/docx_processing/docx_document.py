from .docx.document import Document
from .docx.style import Styles
from .docx.theme import Theme


# pylint: disable-next=too-few-public-methods
class DocxDocument:
    styles: Styles
    document: Document
    theme: Theme

    def __init__(self, document: Document, styles: Styles, theme: Theme):
        self.styles = styles
        self.document = document
        self.theme = theme
