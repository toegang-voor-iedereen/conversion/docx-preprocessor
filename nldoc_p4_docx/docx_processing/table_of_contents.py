from .docx.document import (
    StructuredDocumentTag,
    DocumentPartObject,
    StructuredDocumentTagProperties,
)
from .docx_document import DocxDocument


def strip_table_of_contents(docx: DocxDocument) -> DocxDocument:
    def is_toc(sdt: StructuredDocumentTag) -> bool:
        match sdt:
            case StructuredDocumentTag(
                properties=StructuredDocumentTagProperties(
                    document_part_object=DocumentPartObject(doc_part_gallery="Table of Contents")
                )
            ):
                return True
            case _:
                return False

    for sdt in docx.document.body.structured_document_tags:
        if is_toc(sdt):
            sdt.remove()

    toc_and_tof_styles = [
        s
        for s in docx.styles.styles
        if s.type == "paragraph"
        and (
            s.name.lower() == "table of figures"
            or s.style_id.lower() == "tableoffigures"
            or s.name.lower().startswith("toc")
        )
    ]

    toc_and_tof_descendants = [
        descendant for s in toc_and_tof_styles for descendant in docx.styles.lookup_descendants(s)
    ]

    all_toc_style_ids = [s.style_id for s in toc_and_tof_styles] + toc_and_tof_descendants

    for paragraph in docx.document.body.paragraphs:
        if paragraph.properties and paragraph.properties.style in all_toc_style_ids:
            paragraph.remove()

    return docx
