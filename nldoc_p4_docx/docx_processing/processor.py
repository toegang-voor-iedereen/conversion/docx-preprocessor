import typing

import pydantic

from .docx_document import DocxDocument


class DocxProcessor(pydantic.BaseModel):
    processors: list[typing.Callable[[DocxDocument], DocxDocument]]

    def process(self, docx: DocxDocument) -> DocxDocument:
        for processor in self.processors:
            docx = processor(docx)
        return docx
