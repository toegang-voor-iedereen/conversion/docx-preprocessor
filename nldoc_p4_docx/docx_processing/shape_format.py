import typing
import copy

import pydash

from .docx.document import Paragraph, ParagraphRun, Picture

from .docx_document import DocxDocument


def extract_shape_format(docx: DocxDocument) -> DocxDocument:
    def shape_format_paragraphs_from_run(run: ParagraphRun) -> typing.List[Paragraph]:
        shapes = [
            *pydash.get(run, "alternate_content.fallback.picture.shapes", []),
            *pydash.get(run, "alternate_content.fallback.picture.group.shapes", []),
        ]

        paragraphs: typing.List[Paragraph] = []

        for shape in shapes:
            if shape.imagedata:
                nsmap = shape.xml.getroottree().getroot().nsmap
                image_shape_pict = Picture(nsmap=nsmap)
                image_shape_pict.xml.append(copy.deepcopy(shape.xml))

                image_shape_run = ParagraphRun(nsmap=nsmap)
                image_shape_run.xml.append(image_shape_pict.xml)

                image_shape_paragraph = Paragraph(nsmap=nsmap)
                image_shape_paragraph.xml.append(image_shape_run.xml)

                paragraphs = [*paragraphs, image_shape_paragraph]
            else:
                paragraphs = [*paragraphs, *pydash.get(shape, "textbox.content.paragraphs", [])]

        return paragraphs

    def shape_format_paragraphs(paragraph: Paragraph) -> typing.List[Paragraph]:
        paragraphs: typing.List[Paragraph] = []

        for run in paragraph.runs or []:
            paragraphs = [*paragraphs, *shape_format_paragraphs_from_run(run)]

        return paragraphs

    for p in docx.document.body.paragraphs:
        sf_paragraphs = shape_format_paragraphs(p)

        if len(sf_paragraphs) == 0:
            continue

        index = docx.document.body.xml.index(p.xml)

        for sf_paragraph in sf_paragraphs:
            index += 1
            docx.document.body.xml.insert(index, sf_paragraph.xml)

        for run in p.runs:
            if run.alternate_content:
                run.alternate_content.remove()

    return docx
