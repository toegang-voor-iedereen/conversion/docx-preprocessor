import typing

from .decorators import (
    xml_element_value,
    xml_element_existence,
    xml_attribute,
    xml_element,
    xml_elements,
    xml_model,
)

NS = "{http://schemas.openxmlformats.org/wordprocessingml/2006/main}"


@xml_element_value(f"{NS}color", f"{NS}val", "color")
@xml_element_value(f"{NS}sz", f"{NS}val", "font_size", cast=int)
@xml_element_value(f"{NS}szCs", f"{NS}val", "font_size_complex_script", cast=int)
@xml_element_value(f"{NS}rFonts", f"{NS}ascii", "font")
@xml_element_existence(f"{NS}b", "bold")
@xml_element_existence(f"{NS}bCs", "bold_complex_script")
@xml_element_existence(f"{NS}i", "italic")
@xml_element_existence(f"{NS}iCs", "italic_complex_script")
@xml_element_existence(f"{NS}strike", "strike")
@xml_element_value(f"{NS}u", f"{NS}val", "underscore")
@xml_element_value(f"{NS}vertAlign", f"{NS}val", "vertical_align")
class RunProperties(xml_model(f"{NS}rPr")):
    color: typing.Optional[str]
    font_size: typing.Optional[int]
    font_size_complex_script: typing.Optional[int]
    font: typing.Optional[str]

    bold: bool
    bold_complex_script: bool
    italic: bool
    italic_complex_script: bool
    strike: bool
    underscore: typing.Optional[str]
    vertical_align: typing.Optional[str]


@xml_attribute(f"{NS}styleId", "style_id")
@xml_attribute(f"{NS}type", "type")
@xml_attribute(
    f"{NS}customStyle", "is_custom", required=False, default=False, cast=lambda x: x == "1"
)
@xml_attribute(f"{NS}default", "is_default", required=False, default=False, cast=lambda x: x == "1")
@xml_element_value(f"{NS}name", f"{NS}val", "name")
@xml_element_value(f"{NS}basedOn", f"{NS}val", "based_on")
@xml_element_value(f"{NS}link", f"{NS}val", "link")
@xml_element(f"{NS}rPr", "run_properties", cast=RunProperties)
class Style(xml_model(f"{NS}style")):
    run_properties: RunProperties

    style_id: str
    type: str
    is_custom: bool
    is_default: bool

    name: typing.Optional[str]
    based_on: typing.Optional[str]
    link: typing.Optional[str]


@xml_element(f"{NS}rPr", "run_properties", cast=RunProperties)
class DefaultRunProperties(xml_model(f"{NS}rPrDefault")):
    run_properties: RunProperties


@xml_element(f"{NS}rPrDefault", "default_run_properties", cast=DefaultRunProperties)
class DocumentDefaults(xml_model(f"{NS}docDefaults")):
    default_run_properties: DefaultRunProperties


@xml_element(f"{NS}docDefaults", "document_defaults", cast=DocumentDefaults)
@xml_elements(f"{NS}style", "styles", cast=Style)
class Styles(xml_model(f"{NS}styles")):
    document_defaults: DocumentDefaults
    styles: typing.List[Style]

    def lookup_ancestors(self, style: Style) -> typing.List[Style]:
        if style.based_on is None:
            return []

        matches = [s for s in self.styles if s.style_id == style.based_on]

        assert len(matches) == 1

        return [style] + self.lookup_ancestors(matches[0])

    def lookup_descendants(self, style: Style) -> typing.List[Style]:
        return [
            s2
            for s in self.styles
            for s2 in [s] + self.lookup_ancestors(s)
            if s.based_on == style.style_id
        ]
