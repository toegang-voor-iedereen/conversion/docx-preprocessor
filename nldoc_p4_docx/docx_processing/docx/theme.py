from .decorators import xml_element, xml_attribute, xml_model

NS = "{http://schemas.openxmlformats.org/drawingml/2006/main}"


@xml_attribute("typeface", "typeface")
class FontLatin(xml_model(f"{NS}latin")):
    typeface: str


@xml_element(f"{NS}latin", "latin", cast=FontLatin)
class MajorFont(xml_model(f"{NS}majorFont")):
    latin: FontLatin


@xml_element(f"{NS}latin", "latin", cast=FontLatin)
class MinorFont(xml_model(f"{NS}minorFont")):
    latin: FontLatin


@xml_element(f"{NS}majorFont", "major_font", cast=MajorFont)
@xml_element(f"{NS}minorFont", "minor_font", cast=MinorFont)
class FontScheme(xml_model(f"{NS}fontScheme")):
    major_font: MajorFont
    minor_font: MinorFont


@xml_element(f"{NS}fontScheme", "font_scheme", cast=FontScheme)
class ThemeElements(xml_model(f"{NS}themeElements")):
    font_scheme: FontScheme


@xml_element(f"{NS}themeElements", "theme_elements", cast=ThemeElements)
class Theme(xml_model(f"{NS}theme")):
    theme_elements: ThemeElements
