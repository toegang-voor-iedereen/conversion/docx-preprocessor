import typing
import lxml.etree


def xml_model(name: str):
    class Model:
        xml: lxml.etree.Element

        def __init__(self, xml: typing.Optional[lxml.etree.Element] = None, nsmap=None):
            if xml is None:
                xml = lxml.etree.Element(name, nsmap=nsmap)
            self.xml = xml

        def remove(self):
            parent = self.xml.getparent()

            if parent is None:
                raise ValueError("Cannot remove element without parent")

            parent.remove(self.xml)

    return Model


def xml_attribute(
    attribute_name: str,
    property_name: str,
    cast=str,
    required: bool = True,
    default: typing.Any = None,
):
    def class_decorator(cls):
        def getter(self):
            value = self.xml.get(attribute_name)

            if value is None and required:
                raise ValueError(f"Required Attribute not found: {attribute_name}")

            if value is None:
                return default

            return cast(value)

        def setter(self, value):
            self.xml.set(attribute_name, cast(value))

        setattr(cls, property_name, property(getter, setter))

        return cls

    return class_decorator


def xml_element_value(
    element_name: str,
    attribute_name: str,
    property_name: str,
    cast: typing.Type = str,
    remove_element_if_value_none: bool = True,
):
    def class_decorator(cls):
        def getter(self):
            element = self.xml.find(element_name)

            if element is None:
                return None

            value = element.get(attribute_name)

            if value is None:
                return None

            return cast(value)

        def setter(self, value):
            element = self.xml.find(element_name)

            if value is None and element is not None and remove_element_if_value_none:
                self.xml.remove(element)
                return

            if value is None and element is not None:
                element.attrib.pop(attribute_name)
                return

            if value is None:
                return

            if value is not None:
                value = str(value)

            if element is None:
                element = lxml.etree.Element(element_name)
                self.xml.append(element)

            element.set(attribute_name, value)

        setattr(cls, property_name, property(getter, setter))

        return cls

    return class_decorator


def xml_element_existence(element_name: str, property_name: str):
    def class_decorator(cls):
        def getter(self) -> bool:
            return self.xml.find(element_name) is not None

        def setter(self, value: bool):
            element = self.xml.find(element_name)

            if element is None and value:
                element = lxml.etree.Element(element_name)
                self.xml.append(element)

            if element is not None and not value:
                self.xml.remove(element)

        setattr(cls, property_name, property(getter, setter))

        return cls

    return class_decorator


def xml_contents(property_name: str):
    def class_decorator(cls):
        def getter(self) -> str:
            return self.xml.text

        setattr(cls, property_name, property(getter))

        return cls

    return class_decorator


CastT = typing.TypeVar("CastT")


def xml_element(element_name: str, property_name: str, cast: typing.Type[CastT]):
    def class_decorator(cls):
        def getter(self) -> CastT:
            element = self.xml.find(element_name)

            if element is None:
                return None

            return cast(element)

        def setter(self, value: CastT):
            element = self.xml.find(element_name)

            if element is None:
                self.xml.append(value.xml)
            else:
                self.xml.replace(element, value.xml)

        setattr(cls, property_name, property(getter, setter))

        return cls

    return class_decorator


def xml_elements(
    element_name: typing.Union[str, typing.List[str]], property_name: str, cast: typing.Type[CastT]
):
    def class_decorator(cls):
        def getter(self) -> typing.List[CastT]:
            element_names = [element_name] if isinstance(element_name, str) else element_name

            return [cast(element) for element in self.xml if element.tag in element_names]

        setattr(cls, property_name, property(getter))

        return cls

    return class_decorator
