import typing

from .decorators import (
    xml_element,
    xml_element_value,
    xml_attribute,
    xml_elements,
    xml_model,
    xml_contents,
)
from .style import RunProperties

NS_W = "{http://schemas.openxmlformats.org/wordprocessingml/2006/main}"
NS_MC = "{http://schemas.openxmlformats.org/markup-compatibility/2006}"
NS_VML = "{urn:schemas-microsoft-com:vml}"


@xml_element_value(f"{NS_W}ilvl", f"{NS_W}val", "indent_level", cast=int)
@xml_element_value(f"{NS_W}numId", f"{NS_W}val", "numbering_id", cast=int)
class NumberingProperties(xml_model(f"{NS_W}numPr")):
    indent_level: int
    numbering_id: int


@xml_attribute(f"{NS_W}left", "left", required=False)
@xml_attribute(f"{NS_W}hanging", "hanging", required=False)
class ParagraphIndentProperties(xml_model(f"{NS_W}ind")):
    hanging: typing.Optional[int]
    left: typing.Optional[int]


@xml_element_value(f"{NS_W}pStyle", f"{NS_W}val", "style")
@xml_element(f"{NS_W}numPr", "numbering_properties", cast=NumberingProperties)
@xml_element(f"{NS_W}ind", "indent", cast=ParagraphIndentProperties)
@xml_element(f"{NS_W}rPr", "run_properties", cast=RunProperties)
class ParagraphProperties(xml_model(f"{NS_W}pPr")):
    style: typing.Optional[str]
    numbering_properties: typing.Optional[NumberingProperties]
    indent: typing.Optional[ParagraphIndentProperties]
    run_properties: typing.Optional[RunProperties]


# pylint: disable-next=unnecessary-lambda
@xml_elements(f"{NS_W}p", "paragraphs", cast=lambda x: Paragraph(x))
class TextboxContent(xml_model(f"{NS_VML}txbxContent")):
    paragraphs: typing.List["Paragraph"]


@xml_element(f"{NS_W}txbxContent", "content", cast=TextboxContent)
class Textbox(xml_model(f"{NS_VML}textbox")):
    content: typing.Optional[TextboxContent]


@xml_element(f"{NS_VML}textbox", "textbox", cast=Textbox)
class Rectangle(xml_model(f"{NS_VML}rect")):
    textbox: typing.Optional[Textbox]


@xml_element(f"{NS_VML}textbox", "textbox", cast=Textbox)
class RoundRectangle(xml_model(f"{NS_VML}rect")):
    textbox: typing.Optional[Textbox]


class ImageData(xml_model(f"{NS_VML}imagedata")):
    pass


@xml_element(f"{NS_VML}textbox", "textbox", cast=Textbox)
@xml_element(f"{NS_VML}imagedata", "imagedata", cast=ImageData)
class Shape(xml_model(f"{NS_VML}shape")):
    textbox: typing.Optional[Textbox] = None
    imagedata: typing.Optional[ImageData] = None


@xml_elements([f"{NS_VML}shape", f"{NS_VML}roundrect", f"{NS_VML}rect"], "shapes", cast=Shape)
class VmlGroup(xml_model(f"{NS_VML}group")):
    shapes: typing.List[Shape] = []


@xml_elements([f"{NS_VML}shape", f"{NS_VML}roundrect", f"{NS_VML}rect"], "shapes", cast=Shape)
@xml_element(f"{NS_VML}group", "group", cast=VmlGroup)
class Picture(xml_model(f"{NS_W}pict")):
    shapes: typing.List[Shape] = []
    group: typing.Optional[VmlGroup]


@xml_element(f"{NS_W}pict", "picture", cast=Picture)
class AlternateContentFallback(xml_model(f"{NS_MC}Fallback")):
    picture: typing.Optional[Picture]


@xml_element(f"{NS_MC}Fallback", "fallback", cast=AlternateContentFallback)
class AlternateContent(xml_model(f"{NS_MC}AlternateContent")):
    fallback: typing.Optional[AlternateContentFallback]


@xml_contents("text")
class ParagraphRunText(xml_model("{namespace}t")):
    text: typing.Optional[str]


@xml_element(f"{NS_W}t", "text", cast=ParagraphRunText)
@xml_element(f"{NS_W}rPr", "run_properties", cast=RunProperties)
@xml_element(f"{NS_MC}AlternateContent", "alternate_content", cast=AlternateContent)
@xml_element(f"{NS_W}pict", "picture", cast=Picture)
class ParagraphRun(xml_model(f"{NS_W}r")):
    run_properties: typing.Optional[RunProperties]
    alternate_content: typing.Optional[AlternateContent]
    picture: typing.Optional[Picture]


@xml_element(f"{NS_W}pPr", "properties", cast=ParagraphProperties)
@xml_elements(f"{NS_W}r", "runs", cast=ParagraphRun)
class Paragraph(xml_model(f"{NS_W}p")):
    properties: typing.Optional[ParagraphProperties]
    runs: typing.List[ParagraphRun]


@xml_element_value(f"{NS_W}docPartGallery", f"{NS_W}val", "doc_part_gallery")
class DocumentPartObject(xml_model(f"{NS_W}docPartObj")):
    doc_part_gallery: typing.Optional[str]


@xml_element(f"{NS_W}docPartObj", "document_part_object", cast=DocumentPartObject)
class StructuredDocumentTagProperties(xml_model(f"{NS_W}sdtPr")):
    document_part_object: typing.Optional[DocumentPartObject]


@xml_element(f"{NS_W}sdtPr", "properties", cast=StructuredDocumentTagProperties)
class StructuredDocumentTag(xml_model(f"{NS_W}sdt")):
    properties: typing.Optional[ParagraphProperties]


@xml_elements(f"{NS_W}p", "paragraphs", cast=Paragraph)
@xml_elements(f"{NS_W}sdt", "structured_document_tags", cast=StructuredDocumentTag)
class DocumentBody(xml_model(f"{NS_W}body")):
    paragraphs: typing.List[Paragraph]
    structured_document_tags: typing.List[StructuredDocumentTag]


@xml_element(f"{NS_W}body", "body", cast=DocumentBody)
class Document(xml_model(f"{NS_W}document")):
    body: DocumentBody
