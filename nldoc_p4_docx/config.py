import pydantic_settings

from .nldoc_conversion_worker.config import (
    S3Config,
    QueueConfig,
    AmqpConnectionConfig,
    ExchangeConfig,
)


class Settings(pydantic_settings.BaseSettings):
    model_config = pydantic_settings.SettingsConfigDict(env_nested_delimiter="__")

    s3: S3Config = S3Config()
    amqp: AmqpConnectionConfig = AmqpConnectionConfig()
    queue: QueueConfig = QueueConfig()
    exchange: ExchangeConfig = ExchangeConfig()
