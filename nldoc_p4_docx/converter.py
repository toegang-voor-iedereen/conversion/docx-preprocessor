import os
import random
import string
import tempfile
import typing

from .docx_processing.io import DocxIO
from .nldoc_conversion_worker.converter import Converter
from .nldoc_conversion_worker.dto import ConversionTypeMapping, ConversionMessage, ConversionData
from .nldoc_conversion_worker.error import UnsupportedConversionException
from .content_type import ContentType

from .docx_processing.processor import DocxProcessor
from .docx_processing.table_of_contents import strip_table_of_contents
from .docx_processing.shape_format import extract_shape_format


class DocxConverter(Converter):
    bucket: str
    processor: DocxProcessor

    def __init__(
        self,
        s3,
        bucket: str,
        processor: DocxProcessor = DocxProcessor(
            processors=[strip_table_of_contents, extract_shape_format]
        ),
    ):
        self.s3 = s3
        self.bucket = bucket
        self.processor = processor

    def convert(self, message: ConversionMessage) -> ConversionMessage:
        print(f"Start converter: {message.data.content_location} from bucket {self.bucket}.")
        if not self.supports_message(message):
            mapping = message.conversion_stack[0]
            raise UnsupportedConversionException(mapping.from_content_type, mapping.to_content_type)

        input_path = tempfile.mktemp()
        output_path = tempfile.mktemp()

        print(f"Downloading {message.data.content_location} from bucket {self.bucket}.")

        self.s3.download_file(self.bucket, message.data.content_location, input_path)

        print(f"Converting {message.data.content_location} from bucket {self.bucket}.")

        io = DocxIO(input_path)

        print(f"Converting {message.data.content_location} from bucket {self.bucket}.")

        converted = self.processor.process(io.read())

        print(
            f"Merging processed variant of {message.data.content_location} "
            f"from bucket {self.bucket}."
        )

        io.merge(output_path, converted)

        os.unlink(input_path)

        suffix = "".join(random.choice(string.ascii_letters) for _ in range(10))

        output_key = f"/{message.identifier}/p4.docx.{suffix}"

        print(
            f"Uploading processed variant of {message.data.content_location} "
            f"from bucket {self.bucket} to {output_key}."
        )

        self.s3.upload_file(output_path, self.bucket, output_key)

        os.unlink(output_path)

        return ConversionMessage(
            identifier=message.identifier,
            mapping=message.mapping,
            data=ConversionData(contentLocation=output_key, assets=message.data.assets),
            conversionStack=message.conversion_stack[1:],
            meta=message.meta,
        )

    def supported_conversions(self) -> typing.List[ConversionTypeMapping]:
        return [ConversionTypeMapping(**{"from": ContentType.DOCX, "to": ContentType.DOCX_P4})]
