from .nldoc_conversion_worker.worker import ConversionWorker
from .nldoc_conversion_worker.connection import establish_s3_connection, establish_amqp_connection
from .converter import DocxConverter
from .config import Settings


def configure() -> Settings:
    return Settings()


def create_converter(settings: Settings) -> DocxConverter:
    return DocxConverter(establish_s3_connection(settings.s3), settings.s3.bucket)


def create_worker(settings: Settings, converter: DocxConverter) -> ConversionWorker:
    return ConversionWorker(
        converter=converter,
        connection=establish_amqp_connection(settings.amqp),
        queue=settings.queue,
        exchange=settings.exchange,
    )


def main():
    settings = configure()

    print(f"RUNNING WITH FOLLOWING SETTINGS: {str(settings)}")

    worker = create_worker(settings, create_converter(settings))

    worker.setup()

    worker.consume()
