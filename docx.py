import argparse
import os
import sys
import tempfile
from nldoc_p4_docx.docx_processing.io import DocxIO
from nldoc_p4_docx.docx_processing.processor import DocxProcessor
from nldoc_p4_docx.docx_processing.table_of_contents import strip_table_of_contents
from nldoc_p4_docx.docx_processing.shape_format import extract_shape_format

parser = argparse.ArgumentParser()

parser.add_argument("path")

args = parser.parse_args()

if not os.path.isfile(args.path):
    print("Path is not file")
    sys.exit(1)

processor = DocxProcessor(processors=[strip_table_of_contents, extract_shape_format])

io = DocxIO(args.path)

output_path = tempfile.mktemp()

io.merge(output_path, processor.process(io.read()))

print(output_path)
