# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed

- NLDOC-834: no requeue on nack.

## [0.3.2] - 2023-11-30

# Added

- NLDOC-834: logging worker

## [0.3.1] - 2023-11-30

### Added

- NLDOC-834: logging converter

## [0.3.0] - 2023-11-29

- NLDOC-847: Improve Word Art extraction
- NLDOC-1099: Table of Figures removal

## [0.2.0] - 2023-11-22

### Added

- NLDOC-834: Release Logic

## [0.1.0] - 2023-11-21

Initial release

### Added

- NLDOC-834: Skeleton
- NLDOC-845: Remove Table of Contents
- NLDOC-847: Extract Word Art
