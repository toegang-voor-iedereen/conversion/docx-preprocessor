ARG BASE_IMAGE="registry.gitlab.com/toegang-voor-iedereen/docker-images/python/python-ci-cd:latest"

FROM ${BASE_IMAGE} AS base

WORKDIR /usr/src/app

COPY . .

RUN apk update && apk add --no-cache git libffi-dev openssl-dev build-base
RUN pip3 install "."

CMD ["nldoc-p4-docx-worker"]
