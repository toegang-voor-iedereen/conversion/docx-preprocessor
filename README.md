<div class="panel panel-gitlab-orange">
<strong>Be aware!</strong>

<div class="panel-body">

This project has a strict <strong>100%</strong> coverage requirement.

</div>
</div>


# P4 / PPPP

_P4_ or _PPPP_ stands for _PPPP is a Pandoc Pre-Processor_.

## To Do

- [x] Linting, e.g. by flake8 and/or pylint.
- [x] 100% code coverage by mocking/stubbing RabbitMQ and S3.
- [x] Working RabbitMQ/AMQP bindings.
- [ ] Working Docker Compose setup.

## What is it?

This is a collection of preprocessing packages and services to preprocess both `.docx` and `.odt` files.

## RabbitMQ

### Configuring queues, exchanges and bindings,

Configuring queues, exchanges and bindings is done at boot.

### Testing queues

To be documented

### Listening

To be documented

## Development

### Installing dependencies

Install `mkcert`:

    $ brew install mkcert

Run `.dev/setup.sh` to build images and setup certificates.

    $ bash .dev/setup.sh

Then add to `/etc/hosts`:

    127.0.0.1    s3.php-dcs.local
    127.0.0.1    esc.php-dcs.local
    127.0.0.1    rabbitmq.php-dcs.local

### Building, linting, testing

Setup pre-commit and run:

    $ pre-commit run --all-files

#### Pre-commit

1. Install [pre-commit](https://pre-commit.com/#install)
2. Run following command:

        $ pre-commit install

### Docker

#### Building

Before using `docker-compose up` please run the following script:

    $ bash ./.dev/setup.sh

#### S3

Development using Docker Composer is possible. Bucket should be created
automatically.

To manually create a bucket, run in the S3 container:

    $ awslocal s3api create-bucket --bucket local

Or run from your machine:

    $ aws s3api create-bucket --no-verify-ssl --endpoint-url "http://localhost:4566" --bucket local --region us-east-1

Now you can publish. For example: upload a document (e.g. "sample2.doc") to the
bucket and publish to queue `queue.conversion.inbound`:

```json
{
    "identifier": "abc",
    "mapping": {
        "from": "application/msword",
        "to": "text/html"
    },
    "data": {
        "assets": [],
        "contentLocation": "sample2.doc"
    },
    "conversionStack": null
}
```

The resulting file will be in `queue.publicatietool.outbound`.

## License

Software is licensed under _European Union Public License 1.2_.
