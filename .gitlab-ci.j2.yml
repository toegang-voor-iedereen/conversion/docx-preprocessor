include:
    -   remote: 'https://gitlab.com/toegang-voor-iedereen/docker-images/-/raw/master/template.yml'
    -   remote: 'https://gitlab.com/toegang-voor-iedereen/docker-images/-/raw/master/templates/poetry.yml'

stages:
  - test
  - poetry-deploy
  - docker-build
  - docker-manifest
  - release
  - deploy

test:
    stage: test
    image: registry.gitlab.com/toegang-voor-iedereen/docker-images/python/python-ci-cd:latest
    before_script:
        - poetry install
    script:
        - pre-commit run --all-files
        - poetry run coverage xml
        - poetry run coverage report
    coverage: '/Total\s+\d+\s+\d+\s+(\d+)%/'
    artifacts:
        reports:
            coverage_report:
                coverage_format: cobertura
                path: coverage.xml

poetry-deploy:
    stage: poetry-deploy
    image: registry.gitlab.com/toegang-voor-iedereen/docker-images/python/python-ci-cd:latest
    before_script:
        - !reference [ .commands, poetry, auth ]
    script:
        - poetry publish --build --repository gitlab
    only:
        - tags

.docker-base: &docker-base
    stage: docker-build
    image: registry.gitlab.com/toegang-voor-iedereen/docker-images/docker/docker-ci-cd:latest
    before_script:
        - !reference [ .commands, docker, gitlab, login ]
        - DOCKER_TAG="${CI_COMMIT_REF_NAME}"
        - >
            [ -n "$CI_COMMIT_TAG" ]
            && DOCKER_PUSH=1
            && DOCKER_TAG="${CI_COMMIT_TAG}"
        {%- for environment in deployment_environments if environment.automatic %}
        - >
            [ "$CI_COMMIT_REF_NAME" == "{{ environment.automatic.commit_ref }}" ]
            && DOCKER_TAG="{{ environment.docker_tag }}"
            && DOCKER_PUSH=1
        {%- endfor %}
        - DOCKER_REPOSITORY="${DOCKER_IMAGE_NAME}:${DOCKER_TAG}"
        - echo "CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}"
        - echo "DOCKER_IMAGE_NAME=${DOCKER_IMAGE_NAME}"
        - echo "DOCKER_TAG=${DOCKER_TAG}"
        - echo "DOCKER_REPOSITORY=${DOCKER_REPOSITORY}"
        - echo "DOCKER_PUSH=${DOCKER_PUSH}"

{% for image_short_name, image in images.items() %}
"{{ settings.prefix.docker_manifest }} {{ image_short_name }}":
    <<: *docker-base
    stage: docker-manifest
    variables:
        DOCKER_IMAGE_NAME: '{{ image.name }}'
    script:
        #    [Manifest]
        #    Manifest:         {{ image.name }}
        #    Architectures:    {{ platforms | join(', ') }}
        - >
            docker manifest create
            "${DOCKER_REPOSITORY}"
            {%- for platform in platforms %}
            "${DOCKER_REPOSITORY}-{{ platform }}"
            {%- endfor %}
        - docker manifest push "${DOCKER_REPOSITORY}"
    only:
        - tags
        {%- for environment in deployment_environments if environment.automatic %}
        - {{ environment.automatic.commit_ref }}
        {%- endfor %}
{%- endfor %}

# Start of builds
{%- for image_short_name, image in images.items() %}
{%- for platform in platforms %}
"{{ settings.prefix.docker_build }} {{ image_short_name }}-{{ platform }}":
    <<: *docker-base
    tags: [ '{{ platform }}' ]
    variables:
        DOCKER_PLATFORM: '{{ platform }}'
        DOCKER_IMAGE_NAME: '{{ image.name }}'
    script:
        - DOCKER_FULL_REPOSITORY="${DOCKER_REPOSITORY}-${DOCKER_PLATFORM}"
        - DOCKER_TEST_REPOSITORY="${DOCKER_PLATFORM}-${CI_JOB_ID}"
        - echo "DOCKER_FULL_REPOSITORY=${DOCKER_FULL_REPOSITORY}"
        - echo "DOCKER_TEST_REPOSITORY=${DOCKER_TEST_REPOSITORY}"
        - >
            docker build .
            -t "${DOCKER_TEST_REPOSITORY}"
            --no-cache
            -f "{{ image.Dockerfile }}"
            {%- for arg_key, arg_value in image.arguments.items() %}
            --build-arg "{{ arg_key }}={{ arg_value }}"
            {%- endfor %}
        #- docker run --rm "${DOCKER_TEST_REPOSITORY}" npm run test
        - >
            if [ "$DOCKER_PUSH" == "1" ]; then
                docker tag "${DOCKER_TEST_REPOSITORY}" "${DOCKER_FULL_REPOSITORY}" &&
                docker push "${DOCKER_FULL_REPOSITORY}";
            fi;
        - docker rmi "${DOCKER_TEST_REPOSITORY}"
    {%- if image.depends_on %}
    needs:
        {%- for dependency in image.depends_on %}
        - "{{ settings.prefix.docker_build }} {{ dependency }}-{{ platform }}"
        {%- endfor %}
    {%- endif %}
{% endfor %}
{% endfor %}

release:
    !reference [ .jobs, release ]

# Start of Deployments
{%- for environment in deployment_environments if not environment.automatic %}
"Deploy to {{ environment.name }}":
    <<: *docker-base
    stage: deploy
    script:
        {%- for platform in platforms %}
        {%- for image_short_name, image in images.items() %}
        - >
            docker manifest create
            "{{ image.name }}:{{ environment.docker_tag }}-{{ platform }}"
            "{{ image.name }}:$CI_COMMIT_TAG-{{ platform }}"
        - docker manifest push "{{ image.name }}:{{ environment.docker_tag }}-{{ platform }}"
        {%- endfor %}
        {%- endfor %}
    when: manual
    only:
      - tags
    needs:
    {%- if environment.first_deploy_to %}
        - "Deploy to {{ environment.first_deploy_to }}"
    {%- endif %}
{% endfor -%}
