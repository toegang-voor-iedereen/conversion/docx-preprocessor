## Description

[Provide a brief description of the changes in this MR]

## Coverage

**❗ When merging, please make sure the coverage of the source branch is higher
than the coverage of the destination branch.**

| Direction   | Branch             | Coverage                                                                                                                                         |
|-------------|--------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| Source      | `%{source_branch}` | ![Coverage Badge Source Branch](https://gitlab.com/toegang-voor-iedereen/conversion/docx-preprocessor/badges/%{source_branch}/coverage.svg)      |
| Destination | `%{target_branch}` | ![Coverage Badge Destination Branch](https://gitlab.com/toegang-voor-iedereen/conversion/docx-preprocessor/badges/%{target_branch}/coverage.svg) |

## Definition of Done

- [ ] Code changes are documented (if relevant)
- [ ] Tests pass
- [ ] Code coverage has increased
- [ ] Reviewed
- [ ] Code that has been added is fully covered by tests

---
