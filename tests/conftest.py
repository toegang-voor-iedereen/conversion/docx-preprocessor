import os
import typing

import lxml.etree
import pytest
from syrupy.matchers import path_type

from nldoc_p4_docx.docx_processing.docx_document import DocxDocument
from nldoc_p4_docx.docx_processing.io import DocxIO


@pytest.fixture
def theme_1() -> lxml.etree.Element:
    return load_xml_fixture_to_element('docx/theme/theme_1.xml')


@pytest.fixture
def styles_1() -> lxml.etree.Element:
    return load_xml_fixture_to_element('docx/styles/styles_1.xml')


@pytest.fixture
def styles_2() -> lxml.etree.Element:
    return load_xml_fixture_to_element('docx/styles/styles_2.xml')


@pytest.fixture
def fixture_bytes(request: pytest.FixtureRequest) -> bytes:
    with load_fixture(getattr(request, "param")) as file:
        return file.read()


@pytest.fixture
def xml(request: pytest.FixtureRequest) -> lxml.etree.Element:
    return load_xml_fixture_to_element(getattr(request, "param"))


@pytest.fixture
def docx(request: pytest.FixtureRequest) -> DocxDocument:
    return load_docx_io(request).read()


def load_docx_io(request: pytest.FixtureRequest) -> DocxIO:
    return DocxIO(get_fixture_path(getattr(request, "param")))


@pytest.fixture
def docx_io(request: pytest.FixtureRequest) -> DocxIO:
    return load_docx_io(request)


def load_xml_fixture_to_element(relative_path: str) -> lxml.etree.Element:
    with load_fixture(relative_path) as file:
        return lxml.etree.parse(file).getroot()


def load_fixture(relative_path: str) -> typing.IO[bytes]:
    return open(get_fixture_path(relative_path), 'rb')


def get_fixture_path(relative_path: str) -> str:
    return os.path.join(os.path.dirname(__file__), 'fixtures', relative_path)


def all_fixture_paths(base_path: str) -> typing.List[str]:
    fixtures_root_path = os.path.join(os.path.dirname(__file__), 'fixtures')
    fixtures_path = os.path.join(fixtures_root_path, base_path)

    return [
        os.path.relpath(os.path.join(fixtures_path, file), fixtures_root_path)
        for file in os.listdir(fixtures_path)
    ]


matcher = path_type(types=(lxml.etree._Element,), replacer=lambda element, _: f"<Element {element.tag}>")
