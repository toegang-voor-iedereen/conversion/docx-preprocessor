import lxml.etree
import pytest
from syrupy import snapshot

from nldoc_p4_docx.docx_processing.docx.document import Document
from nldoc_p4_docx.docx_processing.docx.style import Styles
from nldoc_p4_docx.docx_processing.docx.theme import Theme
from nldoc_p4_docx.docx_processing.docx_document import DocxDocument
from nldoc_p4_docx.docx_processing.shape_format import extract_shape_format
from tests.amber_ext import SingleFileAmberSnapshotExtension
from tests.conftest import all_fixture_paths, matcher


@pytest.mark.parametrize('xml', all_fixture_paths("docx/documents"), indirect=True)
def test_shape_format(snapshot, xml: lxml.etree.Element):
    docx = DocxDocument(Document(xml), Styles(), Theme())

    extracted = extract_shape_format(docx).document

    assert [
                lxml.etree.tostring(extracted.xml, pretty_print=True).decode(),
                extracted
           ] == snapshot(
                matcher=matcher,
                extension_class=SingleFileAmberSnapshotExtension
            )
