import pytest
import lxml.etree

from nldoc_p4_docx.docx_processing.docx.document import StructuredDocumentTag, \
    StructuredDocumentTagProperties, DocumentPartObject, Document
from nldoc_p4_docx.docx_processing.docx.style import Styles
from nldoc_p4_docx.docx_processing.docx.theme import Theme
from nldoc_p4_docx.docx_processing.docx_document import DocxDocument
from nldoc_p4_docx.docx_processing.table_of_contents import strip_table_of_contents
from tests.amber_ext import SingleFileAmberSnapshotExtension
from tests.conftest import matcher, all_fixture_paths


@pytest.mark.parametrize('xml', all_fixture_paths("docx/documents"), indirect=True)
def test_do_not_strip_regular_sdt(snapshot, xml: lxml.etree.Element):
    docx = DocxDocument(Document(xml), Styles(), Theme())

    for sdt in docx.document.body.structured_document_tags:
        match sdt:
            case StructuredDocumentTag(
                properties=StructuredDocumentTagProperties(
                    document_part_object=DocumentPartObject()
                )
            ): sdt.properties.document_part_object.doc_part_gallery = "Not a Table of Contents"

    assert strip_table_of_contents(docx).document == snapshot(
        matcher=matcher,
        extension_class=SingleFileAmberSnapshotExtension
    )
