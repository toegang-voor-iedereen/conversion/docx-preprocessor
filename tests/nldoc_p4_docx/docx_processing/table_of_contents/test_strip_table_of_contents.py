import lxml.etree
import pytest
from syrupy import snapshot
from syrupy.matchers import path_type

from nldoc_p4_docx.docx_processing.docx.document import Document, StructuredDocumentTag, \
    StructuredDocumentTagProperties, DocumentPartObject
from nldoc_p4_docx.docx_processing.docx.style import Styles
from nldoc_p4_docx.docx_processing.docx.theme import Theme
from nldoc_p4_docx.docx_processing.docx_document import DocxDocument
from nldoc_p4_docx.docx_processing.io import xml_to_string
from nldoc_p4_docx.docx_processing.table_of_contents import strip_table_of_contents
from tests.amber_ext import SingleFileAmberSnapshotExtension
from tests.conftest import all_fixture_paths, matcher


@pytest.mark.parametrize('xml', all_fixture_paths("docx/documents"), indirect=True)
def test_strip_table_of_contents(snapshot, xml: lxml.etree.Element):
    docx = DocxDocument(Document(xml), Styles(), Theme())

    assert strip_table_of_contents(docx).document == snapshot(
        matcher=matcher,
        extension_class=SingleFileAmberSnapshotExtension
    )
