import pytest
from syrupy import snapshot

from nldoc_p4_docx.docx_processing.docx_document import DocxDocument
from nldoc_p4_docx.docx_processing.processor import DocxProcessor
from nldoc_p4_docx.docx_processing.table_of_contents import strip_table_of_contents
from nldoc_p4_docx.docx_processing.shape_format import extract_shape_format
from tests.amber_ext import SingleFileAmberSnapshotExtension
from tests.conftest import matcher, all_fixture_paths


@pytest.mark.parametrize('docx', all_fixture_paths("docx/docx"), indirect=True)
def test_processor(snapshot, docx: DocxDocument):
    processor = DocxProcessor(processors=[strip_table_of_contents, extract_shape_format])

    processed = processor.process(docx)

    assert [processed, processed.document.xml] == snapshot(
        matcher=matcher,
        extension_class=SingleFileAmberSnapshotExtension
    )
