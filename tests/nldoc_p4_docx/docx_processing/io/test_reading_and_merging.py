import tempfile
import zipfile

import pytest
import lxml.etree

from nldoc_p4_docx.docx_processing.io import DocxIO


def xml_to_string(element: lxml.etree.Element) -> bytes:
    return b'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + b"\r\n" + lxml.etree.tostring(element,
                                                                                            xml_declaration=False)


@pytest.mark.parametrize('docx_io', ['docx/docx/small.docx'], indirect=True)
def test_reading_and_merging(docx_io: DocxIO):
    output_path = tempfile.mktemp()

    docx = docx_io.read()

    assert len(docx.styles.styles) > 0
    assert len(docx.document.body.paragraphs) > 0

    docx.styles.styles[0].name = "Test"
    docx.document.body.paragraphs[0].style = "Test2"

    docx_io.merge(output_path, docx)

    with docx_io.open() as input_zip:
        with zipfile.ZipFile(output_path, "r") as output_zip:
            input_namelist = input_zip.namelist()
            output_namelist = output_zip.namelist()

            input_namelist.sort()
            output_namelist.sort()

            assert input_namelist == output_namelist

            for name in output_namelist:
                output_content = output_zip.read(name).decode()

                if name == DocxIO.PATH_DOCUMENT:
                    expected_content = xml_to_string(docx.document.xml).decode()
                elif name == DocxIO.PATH_STYLES:
                    expected_content = xml_to_string(docx.styles.xml).decode()
                else:
                    expected_content = input_zip.read(name).decode()

                assert output_content == expected_content, f"in file {name}"
