import lxml.etree
from syrupy import snapshot
from syrupy.matchers import path_type

from nldoc_p4_docx.docx_processing.docx.theme import Theme
from tests.conftest import matcher


def test_theme_parsing(snapshot, theme_1: lxml.etree.Element):
    theme = Theme(theme_1)

    assert theme.theme_elements.font_scheme.major_font.latin.typeface == 'Calibri Light'
    assert theme.theme_elements.font_scheme.minor_font.latin.typeface == 'Calibri'

    assert theme == snapshot(matcher=matcher)
