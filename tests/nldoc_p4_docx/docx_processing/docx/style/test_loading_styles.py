import lxml.etree
import pytest

from nldoc_p4_docx.docx_processing.docx.style import Styles
from tests.conftest import all_fixture_paths, matcher


@pytest.mark.parametrize('xml', all_fixture_paths("docx/styles"), indirect=True)
def test_loading_styles(snapshot, xml: lxml.etree.Element):
    assert Styles(xml) == snapshot(matcher=matcher)
