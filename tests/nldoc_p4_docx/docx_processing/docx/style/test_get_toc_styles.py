import lxml.etree
import pytest

from nldoc_p4_docx.docx_processing.docx.style import Styles, RunProperties
from tests.conftest import all_fixture_paths, matcher


@pytest.mark.parametrize('xml', all_fixture_paths("docx/styles"), indirect=True)
def test_get_toc_styles(snapshot, xml: lxml.etree.Element):
    styles = Styles(xml)

    toc_styles = [s for s in styles.styles if
                  s.name.lower().startswith("toc ") and s.type == 'paragraph']

    toc_ancestors = {s.style_id: styles.lookup_ancestors(s) for s in toc_styles}

    assert {"toc_styles": toc_styles, "toc_ancestors": toc_ancestors} == snapshot(matcher=matcher)
