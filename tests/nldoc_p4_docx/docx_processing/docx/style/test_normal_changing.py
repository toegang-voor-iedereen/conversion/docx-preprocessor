import lxml.etree
import pytest

from nldoc_p4_docx.docx_processing.docx.style import Styles, RunProperties
from tests.conftest import all_fixture_paths


@pytest.mark.parametrize('xml', all_fixture_paths("docx/styles"), indirect=True)
def test_normal_changing(snapshot, xml: lxml.etree.Element):
    styles = Styles(xml)

    matches = [s for s in styles.styles if s.name == 'heading 1' and s.type == 'paragraph']

    assert len(matches) == 1

    style = matches[0]

    style.style_id = 'style id xyz'
    style.type = 'paragraph xyz'

    style.name = 'x'
    style.based_on = None
    style.link = None

    if style.run_properties is None:
        style.run_properties = RunProperties()

    style.run_properties.font_size = None
    style.run_properties.font_size_complex_script = None
    style.run_properties.font = None
    style.run_properties.color = None

    style.run_properties.bold = False
    style.run_properties.bold_complex_script = False
    style.run_properties.italic = False
    style.run_properties.italic_complex_script = False
    style.run_properties.strike = False
    style.run_properties.underscore = None
    style.run_properties.vertical_align = None

    assert snapshot == lxml.etree.tostring(styles.xml, pretty_print=True).decode()
