import lxml.etree
import pytest

from nldoc_p4_docx.docx_processing.docx.style import Styles, RunProperties
from tests.conftest import all_fixture_paths, matcher


@pytest.mark.parametrize('xml', all_fixture_paths("docx/styles"), indirect=True)
def test_heading_1_changing(snapshot, xml: lxml.etree.Element):
    styles = Styles(xml)

    matches = [s for s in styles.styles if s.name == 'heading 1' and s.type == 'paragraph']

    assert len(matches) == 1

    style = matches[0]

    style.style_id = 'Heading1 xyz'
    style.type = 'paragraph xyz'

    style.name = 'heading 1 xyz'
    style.based_on = 'Normal xyz'
    style.link = "Heading1CharXyz"

    if style.run_properties is None:
        style.run_properties = RunProperties()

    style.run_properties.font_size = 1234
    style.run_properties.font_size_complex_script = 1234
    style.run_properties.font = 'Font is also Xyz'
    style.run_properties.color = "XYZXYZ"

    style.run_properties.bold = True
    style.run_properties.bold_complex_script = True
    style.run_properties.italic = True
    style.run_properties.italic_complex_script = True
    style.run_properties.strike = True
    style.run_properties.underscore = 'wave'
    style.run_properties.vertical_align = 'idk'

    assert snapshot == lxml.etree.tostring(styles.xml, pretty_print=True).decode()
