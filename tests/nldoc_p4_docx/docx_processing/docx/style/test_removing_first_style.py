import lxml.etree
import pytest

from nldoc_p4_docx.docx_processing.docx.style import Styles
from tests.conftest import all_fixture_paths, matcher


@pytest.mark.parametrize('xml', all_fixture_paths("docx/styles"), indirect=True)
def test_removing_first_style(snapshot, xml: lxml.etree.Element):
    styles = Styles(xml)

    style = styles.styles[0]

    style.remove()

    assert styles == snapshot(matcher=matcher)
