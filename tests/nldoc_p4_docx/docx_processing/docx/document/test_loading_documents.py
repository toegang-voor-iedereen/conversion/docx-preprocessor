import lxml.etree
import pytest
from syrupy import snapshot

from nldoc_p4_docx.docx_processing.docx.document import Document
from tests.amber_ext import SingleFileAmberSnapshotExtension
from tests.conftest import all_fixture_paths, matcher


@pytest.mark.parametrize('xml', all_fixture_paths('docx/documents'), indirect=True)
def test_loading_documents(snapshot, xml: lxml.etree.Element):
    assert Document(xml) == snapshot(matcher=matcher,
                                     extension_class=SingleFileAmberSnapshotExtension)
