import pytest

from nldoc_p4_docx.docx_processing.docx.decorators import xml_model, xml_attribute


def test_xml_attribute_required_but_not_set():
    @xml_attribute("attribute", "attribute", required=True)
    class Model(xml_model("Model")):
        attribute: str

    data = Model()

    with pytest.raises(ValueError):
        data.attribute
