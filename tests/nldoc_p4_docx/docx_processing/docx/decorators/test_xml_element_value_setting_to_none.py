import lxml.etree

from nldoc_p4_docx.docx_processing.docx.decorators import xml_model, xml_element_value


def test_xml_element_value_setting_to_none():
    @xml_element_value(
        "Element",
        "element",
        "element",
        remove_element_if_value_none=False
    )
    class Model(xml_model("Model")):
        element: str

    data = Model(lxml.etree.fromstring("<Model><Element element='value'/></Model>"))

    data.element = None

    assert "<Model><Element/></Model>" == lxml.etree.tostring(data.xml).decode()
