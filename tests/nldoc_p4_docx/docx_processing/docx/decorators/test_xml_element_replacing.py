import lxml.etree

from nldoc_p4_docx.docx_processing.docx.decorators import xml_model, xml_attribute, xml_element


def test_xml_element_replacing():
    @xml_attribute("attribute", "attribute", required=True)
    class ModelElement(xml_model("Element")):
        attribute: str

    @xml_element(element_name="Element", property_name="element", cast=ModelElement)
    class Model(xml_model("Element")):
        element: ModelElement

    data = Model(lxml.etree.fromstring("<Model><Element attribute='value'/></Model>"))

    assert isinstance(data.element, ModelElement)
    assert data.element.attribute == "value"

    data.element = Model(lxml.etree.fromstring("<Element attribute='other element'/>"))

    assert '<Model><Element attribute="other element"/></Model>' == lxml.etree.tostring(
        data.xml
    ).decode()

    assert isinstance(data.element, ModelElement)
    assert data.element.attribute == "other element"
