import pytest

from nldoc_p4_docx.docx_processing.docx.decorators import xml_model


def test_xml_model_remove_without_parent():
    model = xml_model("Model")

    some_model = model()

    with pytest.raises(ValueError):
        some_model.remove()
