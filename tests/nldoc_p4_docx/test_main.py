from unittest import mock

import pika

from nldoc_p4_docx.config import Settings
from nldoc_p4_docx.main import configure, create_converter, create_worker, main

import os

from nldoc_p4_docx.nldoc_conversion_worker.config import ExchangeConfig, QueueConfig, S3Config, \
    AmqpConnectionConfig


def test_main_configure_default(snapshot):
    assert snapshot == configure()


sample_settings = Settings(
    s3=S3Config(
        endpoint_url="https://s3/some-endpoint",
        bucket="some-bucket",
        access_key="some-access-key",
        secret_key="some-secret-key",
        region="some-region"
    ),
    amqp=AmqpConnectionConfig(
        host="rabbitmq_host",
        port=1234,
        vhost="/some/vhost",
        username="some_user",
        password="some_password",
    ),
    queue=QueueConfig(name="some-queue"),
    exchange=ExchangeConfig(conversion="ex.conversion", dlx="ex.dlx", outbound="ex.outbound")
)

custom_env_vars = {
    "S3__ENDPOINT_URL": "https://s3/some-endpoint",
    "S3__BUCKET": "some-bucket",
    "S3__ACCESS_KEY": "some-access-key",
    "S3__SECRET_KEY": "some-secret-key",
    "S3__REGION": "some-region",

    "AMQP__HOST": "rabbitmq_host",
    "AMQP__PORT": "1234",
    "AMQP__VHOST": "/some/vhost",
    "AMQP__USERNAME": "some_user",
    "AMQP__PASSWORD": "some_password",

    "QUEUE__NAME": "some-queue",

    "EXCHANGE__CONVERSION": "ex.conversion",
    "EXCHANGE__DLX": "ex.dlx",
    "EXCHANGE__OUTBOUND": "ex.outbound",
}


def test_main_configure_custom_env():
    with mock.patch.dict("os.environ", custom_env_vars):
        assert sample_settings == configure()


@mock.patch("nldoc_p4_docx.nldoc_conversion_worker.connection.boto3")
@mock.patch("nldoc_p4_docx.main.DocxConverter")
def test_main_create_converter(mock_converter, mock_boto3):
    client = mock.MagicMock()
    mock_boto3.client = mock.MagicMock(return_value=client)

    create_converter(settings=sample_settings)

    mock_boto3.client.assert_called_once_with(
        "s3",
        endpoint_url="https://s3/some-endpoint",
        aws_access_key_id="some-access-key",
        aws_secret_access_key="some-secret-key",
        region_name="some-region"
    )

    mock_converter.assert_called_once_with(client, "some-bucket")


@mock.patch("nldoc_p4_docx.main.DocxConverter")
@mock.patch("nldoc_p4_docx.main.ConversionWorker")
@mock.patch("nldoc_p4_docx.nldoc_conversion_worker.connection.pika.BlockingConnection")
def test_create_worker(mock_pika_bc, mock_conversion_worker, mock_converter):
    amqp_connection = mock.MagicMock()
    mock_pika_bc.return_value = amqp_connection

    create_worker(sample_settings, mock_converter)

    mock_pika_bc.assert_called_once_with(
        pika.ConnectionParameters(
            host="rabbitmq_host",
            port=1234,
            virtual_host="/some/vhost",
            credentials=pika.PlainCredentials(
                username="some_user",
                password="some_password",
            ),
        )
    )

    mock_conversion_worker.assert_called_once_with(
        converter=mock_converter,
        connection=amqp_connection,
        queue=QueueConfig(name="some-queue"),
        exchange=ExchangeConfig(conversion="ex.conversion", dlx="ex.dlx", outbound="ex.outbound"),
    )


@mock.patch("nldoc_p4_docx.main.DocxConverter")
@mock.patch("nldoc_p4_docx.main.ConversionWorker")
@mock.patch("nldoc_p4_docx.nldoc_conversion_worker.connection.pika.BlockingConnection")
@mock.patch("nldoc_p4_docx.nldoc_conversion_worker.connection.boto3")
def test_main(mock_boto3, mock_pika_bc, mock_worker, mock_converter):
    converter = mock.MagicMock()
    mock_converter.return_value = converter

    worker = mock.MagicMock()
    mock_worker.return_value = worker

    amqp_connection = mock.MagicMock()
    mock_pika_bc.return_value = amqp_connection

    s3_client = mock.MagicMock()
    mock_boto3.client = mock.MagicMock(return_value=s3_client)

    with mock.patch.dict("os.environ", custom_env_vars):
        main()

    mock_boto3.client.assert_called_once_with(
        "s3",
        endpoint_url="https://s3/some-endpoint",
        aws_access_key_id="some-access-key",
        aws_secret_access_key="some-secret-key",
        region_name="some-region"
    )

    mock_converter.assert_called_once_with(s3_client, "some-bucket")

    mock_worker.assert_called_once_with(
        converter=converter,
        connection=amqp_connection,
        queue=QueueConfig(name="some-queue"),
        exchange=ExchangeConfig(conversion="ex.conversion", dlx="ex.dlx", outbound="ex.outbound"),
    )

    worker.setup.assert_called_once()
    worker.consume.assert_called_once()
