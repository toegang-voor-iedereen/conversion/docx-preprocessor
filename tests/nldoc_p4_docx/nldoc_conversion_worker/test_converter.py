import typing

from nldoc_p4_docx.nldoc_conversion_worker.converter import Converter
from nldoc_p4_docx.nldoc_conversion_worker.dto import ConversionTypeMapping, ConversionMessage


class ExampleConverter(Converter):
    def supported_conversions(self) -> typing.List[ConversionTypeMapping]:
        return [
            ConversionTypeMapping(**{'from': 'abc', 'to': 'xyz'})
        ]

    def convert(self, message: ConversionMessage) -> ConversionMessage:
        message.conversion_stack.pop(0)
        return message


def test_method_supports():
    converter = ExampleConverter()

    example_message = ConversionMessage(**{
        'identifier': '123456',
        'mapping': {"from": 'abc', "to": "123"},
        'data': {'contentLocation': 'loc', 'assets': []},
        'conversionStack': [
            {"from": 'abc', "to": "xyz"},
            {"from": 'xyz', "to": "123"}
        ]
    })

    assert converter.supports_conversion(example_message.conversion_stack[0]) is True
    assert converter.supports_message(example_message) is True

    example_message.conversion_stack.pop(0)

    assert converter.supports_conversion(example_message.conversion_stack[0]) is False
    assert converter.supports_message(example_message) is False
