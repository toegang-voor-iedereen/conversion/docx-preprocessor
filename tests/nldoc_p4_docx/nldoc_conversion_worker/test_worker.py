import json
from unittest import mock
from unittest.mock import MagicMock, Mock

import pika
import pytest

from nldoc_p4_docx.nldoc_conversion_worker.config import QueueConfig, ExchangeConfig
from nldoc_p4_docx.nldoc_conversion_worker.converter import Converter
from nldoc_p4_docx.nldoc_conversion_worker.dto import ConversionTypeMapping, ConversionMessage, \
    ConversionData
from nldoc_p4_docx.nldoc_conversion_worker.error import UnsupportedConversionException
from nldoc_p4_docx.nldoc_conversion_worker.worker import ConversionWorker

queue_config = QueueConfig(name="queue")
exchange_config = ExchangeConfig(
    dlx="dlx",
    conversion="conversion",
    outbound="outbound",
    unrouted="unrouted"
)


def channel_mock():
    channel = MagicMock()

    channel.exchange_declare = Mock(return_value=None)
    channel.queue_declare = Mock(return_value=None)
    channel.queue_bind = Mock(return_value=None)
    channel.basic_qos = Mock(return_value=None)
    channel.basic_consume = Mock(return_value=None)
    channel.start_consuming = Mock(return_value=None)
    channel.stop_consuming = Mock(return_value=None)

    return channel


class ContextManagerMock(object):
    def __init__(self, value):
        self.value = value

    def __enter__(self):
        return self.value

    def __exit__(self, *args):
        pass


def test_worker_setup():
    converter = mock.MagicMock()
    connection = mock.MagicMock()

    converter.supported_conversions = Mock(return_value=[
        ConversionTypeMapping(**{"from": "abc", "to": "xyz"}),
        ConversionTypeMapping(**{"from": "123", "to": "456"}),
    ])

    channel = channel_mock()
    connection.channel = Mock(return_value=ContextManagerMock(channel))
    ConversionWorker(converter, connection, queue_config, exchange_config).setup()

    channel.exchange_declare.assert_has_calls([
        mock.call(exchange="unrouted", exchange_type='fanout', durable=True,),
        mock.call(exchange="outbound", exchange_type='fanout', durable=True),
        mock.call(
            exchange="conversion",
            exchange_type='direct',
            durable=True,
            arguments={"alternate-exchange": "unrouted"}
        ),
    ])

    channel.queue_declare.assert_called_once_with(
        queue="queue",
        durable=True,
        auto_delete=False,
        arguments={"x-dead-letter-exchange": "dlx"}
    )

    channel.queue_bind.assert_has_calls(
        [
            mock.call(exchange="conversion", queue="queue", routing_key="ZnJvbTphYmM7dG86eHl6"),
            mock.call(exchange="conversion", queue="queue", routing_key="ZnJvbToxMjM7dG86NDU2"),
        ],
        any_order=True
    )


def test_worker_consume():
    converter = mock.MagicMock()
    connection = mock.MagicMock()

    channel = channel_mock()
    connection.channel = Mock(return_value=ContextManagerMock(channel))

    worker = ConversionWorker(converter, connection, queue_config, exchange_config)
    worker.consume()

    channel.basic_qos.assert_called_once_with(prefetch_count=1)
    channel.basic_consume.assert_called_once_with("queue", worker.handle)

    channel.start_consuming.assert_called_once()


def test_worker_consume_keyboard_interrupt():
    converter = mock.MagicMock()
    connection = mock.MagicMock()

    channel = channel_mock()
    channel.start_consuming = Mock(side_effect=KeyboardInterrupt)

    connection.channel = Mock(return_value=ContextManagerMock(channel))

    worker = ConversionWorker(converter, connection, queue_config, exchange_config)
    worker.consume()

    channel.basic_qos.assert_called_once_with(prefetch_count=1)
    channel.basic_consume.assert_called_once_with("queue", worker.handle)

    channel.start_consuming.assert_called_once()

    channel.stop_consuming.assert_called_once()

    connection.close.assert_called_once()


def test_worker_handle_to_outbound():
    converter = mock.MagicMock()
    connection = mock.MagicMock()
    msg_method = mock.MagicMock()
    properties = mock.MagicMock()

    converter.convert = MagicMock(return_value=ConversionMessage(
        identifier='test',
        mapping=ConversionTypeMapping(**{"from": "abc", "to": "xyz"}),
        data=ConversionData(
            contentLocation="location",
            assets=[]
        ),
        conversionStack=[],
        meta={}
    ))

    channel = channel_mock()

    worker = ConversionWorker(converter, connection, queue_config, exchange_config)
    worker.handle(
        channel,
        msg_method,
        properties,
        b"""
        {
            "identifier": "lol",
            "mapping": {"from": "abc", "to": "xyz"},
            "data": {"contentLocation": "location_in", "assets": []},
            "conversionStack": [{"from": "abc", "to": "xyz"}],
            "meta": {}
        }
        """
    )

    converter.convert.assert_called_once_with(
        ConversionMessage(
            identifier='lol',
            mapping=ConversionTypeMapping(**{"from": "abc", "to": "xyz"}),
            data=ConversionData(
                contentLocation="location_in",
                assets=[]
            ),
            conversionStack=[ConversionTypeMapping(**{"from": "abc", "to": "xyz"})],
            meta={}
        )
    )

    channel.basic_publish.assert_called_once_with(
        "outbound",
        "",
        json.dumps(
            json.loads(
                b"""
                {"identifier": "test",
                    "mapping": {"from": "abc", "to": "xyz"},
                    "data": {"assets": [], "contentLocation": "location"},
                    "conversionStack": [],
                    "meta": {}
                }
                """
            )
        ).encode(),
        properties=pika.spec.BasicProperties(
            delivery_mode=pika.DeliveryMode.Persistent,
            content_type="application/json",
        )
    )

    channel.basic_ack.assert_called_once_with(delivery_tag=msg_method.delivery_tag)


def test_worker_handle_to_exchange():
    converter = mock.MagicMock()
    connection = mock.MagicMock()
    msg_method = mock.MagicMock()
    properties = mock.MagicMock()

    converter.convert = MagicMock(return_value=ConversionMessage(
        identifier='test',
        mapping=ConversionTypeMapping(**{"from": "abc", "to": "xyz"}),
        data=ConversionData(
            contentLocation="location_out",
            assets=[]
        ),
        conversionStack=[ConversionTypeMapping(**{"from": "123", "to": "xyz"})],
        meta={}
    ))

    channel = channel_mock()

    worker = ConversionWorker(converter, connection, queue_config, exchange_config)
    worker.handle(
        channel,
        msg_method,
        properties,
        b"""
        {
            "identifier": "lol",
            "mapping": {"from": "abc", "to": "xyz"},
            "data": {"contentLocation": "location_in", "assets": []},
            "conversionStack": [
                {"from": "abc", "to": "123"},
                {"from": "123", "to": "xyz"}
            ],
            "meta": {}
        }
        """
    )

    converter.convert.assert_called_once_with(
        ConversionMessage(
            identifier='lol',
            mapping=ConversionTypeMapping(**{"from": "abc", "to": "xyz"}),
            data=ConversionData(
                contentLocation="location_in",
                assets=[]
            ),
            conversionStack=[
                ConversionTypeMapping(**{"from": "abc", "to": "123"}),
                ConversionTypeMapping(**{"from": "123", "to": "xyz"}),
            ],
            meta={}
        )
    )

    channel.basic_publish.assert_called_once_with(
        "conversion",
        "ZnJvbToxMjM7dG86eHl6",
        json.dumps(
            json.loads(
                b"""
                {"identifier": "test",
                    "mapping": {"from": "abc", "to": "xyz"},
                    "data": {"assets": [], "contentLocation": "location_out"},
                    "conversionStack": [{"from": "123", "to": "xyz"}],
                    "meta": {}
                }
                """
            )
        ).encode(),
        properties=pika.spec.BasicProperties(
            delivery_mode=pika.DeliveryMode.Persistent,
            content_type="application/json",
        )
    )

    channel.basic_ack.assert_called_once_with(delivery_tag=msg_method.delivery_tag)


def test_worker_handle_conversion_fails():
    converter = mock.MagicMock()
    connection = mock.MagicMock()
    msg_method = mock.MagicMock()

    msg_method.routing_key = "some routing key"

    converter.convert = MagicMock(side_effect=UnsupportedConversionException("abc", "xyz"))

    channel = channel_mock()

    body = b"""
        {
            "identifier": "lol2",
            "mapping": {"from": "abc", "to": "xyz"},
            "data": {"contentLocation": "location_in", "assets": []},
            "conversionStack": [
                {"from": "abc", "to": "123"},
                {"from": "123", "to": "xyz"}
            ],
            "meta": {}
        }
        """

    worker = ConversionWorker(converter, connection, queue_config, exchange_config)

    with pytest.raises(UnsupportedConversionException):
        worker.handle(
            channel,
            msg_method,
            pika.spec.BasicProperties(
                headers={
                    "x-failure-count": "1",
                    "x-max-failures": "3",
                    "x-failure-reason-1": "butter",
                    "x-some-header": "some-value"
                }
            ),
            body
        )

    converter.convert.assert_called_once_with(
        ConversionMessage(
            identifier='lol2',
            mapping=ConversionTypeMapping(**{"from": "abc", "to": "xyz"}),
            data=ConversionData(
                contentLocation="location_in",
                assets=[]
            ),
            conversionStack=[
                ConversionTypeMapping(**{"from": "abc", "to": "123"}),
                ConversionTypeMapping(**{"from": "123", "to": "xyz"}),
            ],
            meta={}
        )
    )

    channel.basic_publish.assert_called_once_with(
        exchange="conversion",
        routing_key="some routing key",
        body=body,
        properties=pika.spec.BasicProperties(
            delivery_mode=pika.DeliveryMode.Persistent,
            content_type="application/json",
            headers={
                "x-failure-count": 2,
                "x-max-failures": 3,
                "x-failure-reason-1": "butter",
                "x-some-header": "some-value",
                "x-failure-reason-2": "Unsupported conversion from 'abc' to 'xyz'.",
            }
        )
    )

    channel.basic_ack.assert_called_once_with(delivery_tag=msg_method.delivery_tag)


def test_worker_max_failures_reached():
    converter = mock.MagicMock()
    connection = mock.MagicMock()
    msg_method = mock.MagicMock()
    properties = mock.MagicMock()

    properties.headers = {
        "x-failure-count": "3",
        "x-max-failures": "3",
        "x-retry-reason-1": "butter",
        "x-retry-reason-2": "cheese",
        "x-retry-reason-3": "eggs",
    }

    channel = channel_mock()

    worker = ConversionWorker(converter, connection, queue_config, exchange_config)
    worker.handle(
        channel,
        msg_method,
        properties,
        b"""
        {
            "identifier": "lol",
            "mapping": {"from": "abc", "to": "xyz"},
            "data": {"contentLocation": "location_in", "assets": []},
            "conversionStack": [
                {"from": "abc", "to": "123"},
                {"from": "123", "to": "xyz"}
            ],
            "meta": {}
        }
        """
    )

    channel.basic_nack.assert_called_once_with(delivery_tag=msg_method.delivery_tag, requeue=False)

    channel.stop_consuming.assert_not_called()
    connection.close.assert_not_called()
