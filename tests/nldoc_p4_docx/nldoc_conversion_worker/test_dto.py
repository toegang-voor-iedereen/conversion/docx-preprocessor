import typing

import pydantic
import pytest

from nldoc_p4_docx.nldoc_conversion_worker.dto import ConversionTypeMapping, Asset, \
    ConversionData, ConversionMessage

valid_conversion_message_dict = {
    "identifier": "990b1738-80e6-45f6-9a89-a731cf4d27e5",
    "mapping": {
        "from": "abc",
        "to": "xyz"
    },
    'data': {
        'assets': [
            {'filename': 'file_a.txt', 'contentLocation': 'path/on/s3/1'},
            {'filename': 'file_b.txt', 'contentLocation': 'path/on/s3/2'},
            {'filename': 'file_c.txt', 'contentLocation': 'path/on/s3/3'},
        ],
        'contentLocation': 'other/path/on/s3'
    },
    'conversionStack': [
        {"from": "abc", "to": "123"},
        {"from": "123", "to": "456"},
        {"from": "456", "to": "xyz"}
    ]
}


def test_dto_conversion_type_mapping():
    assert is_conversion_type_mapping(ConversionTypeMapping(**valid_conversion_message_dict['mapping']),
                                      expected_from='abc',
                                      expected_to='xyz')


def test_dto_conversion_type_mapping_empty_from():
    with pytest.raises(pydantic.ValidationError) as exc:
        ConversionTypeMapping(**{"from": "", "to": "abc"})

    errors = exc.value.errors()

    assert len(errors) == 1
    assert errors == [{'type': 'string_too_short',
                       'loc': ('from',),
                       'msg': 'String should have at least 1 character',
                       'input': '',
                       'ctx': {'min_length': 1},
                       'url': 'https://errors.pydantic.dev/2.5/v/string_too_short'}]


def test_dto_conversion_type_mapping_empty_to():
    with pytest.raises(pydantic.ValidationError) as exc:
        ConversionTypeMapping(**{"from": "abc", "to": ""})

    errors = exc.value.errors()

    assert errors == [{'type': 'string_too_short',
                       'loc': ('to',),
                       'msg': 'String should have at least 1 character',
                       'input': '',
                       'ctx': {'min_length': 1},
                       'url': 'https://errors.pydantic.dev/2.5/v/string_too_short'}]


def test_dto_asset():
    instance = Asset(filename="test_dto.py", contentLocation="path/on/s3")

    assert instance.filename == 'test_dto.py'
    assert instance.content_location == 'path/on/s3'

    assert instance.model_dump(by_alias=True) == {"filename": 'test_dto.py', 'contentLocation': 'path/on/s3'}


def test_dto_asset_empty_filename():
    with pytest.raises(pydantic.ValidationError) as exc:
        Asset(filename="", contentLocation="path/on/s3")

    errors = exc.value.errors()

    assert errors == [{'type': 'string_too_short',
                       'loc': ('filename',),
                       'msg': 'String should have at least 1 character',
                       'input': '',
                       'ctx': {'min_length': 1},
                       'url': 'https://errors.pydantic.dev/2.5/v/string_too_short'}]


def test_dto_asset_empty_content_location():
    with pytest.raises(pydantic.ValidationError) as exc:
        Asset(filename="file.txt", contentLocation="")

    errors = exc.value.errors()

    assert errors == [{'type': 'string_too_short',
                       'loc': ('contentLocation',),
                       'msg': 'String should have at least 1 character',
                       'input': '',
                       'ctx': {'min_length': 1},
                       'url': 'https://errors.pydantic.dev/2.5/v/string_too_short'}]


def test_dto_conversion_data():
    instance = ConversionData(**valid_conversion_message_dict['data'])

    assert instance.content_location == 'other/path/on/s3'

    assert len(instance.assets) == 3

    assert isinstance(instance.assets[0], Asset)
    assert instance.assets[0].filename == 'file_a.txt'
    assert instance.assets[0].content_location == 'path/on/s3/1'

    assert isinstance(instance.assets[1], Asset)
    assert instance.assets[1].filename == 'file_b.txt'
    assert instance.assets[1].content_location == 'path/on/s3/2'

    assert isinstance(instance.assets[2], Asset)
    assert instance.assets[2].filename == 'file_c.txt'
    assert instance.assets[2].content_location == 'path/on/s3/3'

    assert instance.model_dump(by_alias=True) == valid_conversion_message_dict['data']


def test_dto_conversion_data_empty_content_location():
    with pytest.raises(pydantic.ValidationError) as exc:
        conversion_data_dict = valid_conversion_message_dict['data'].copy()
        conversion_data_dict['contentLocation'] = ""

        ConversionData(**conversion_data_dict)

    errors = exc.value.errors()

    assert errors == [{'type': 'string_too_short',
                       'loc': ('contentLocation',),
                       'msg': 'String should have at least 1 character',
                       'input': '',
                       'ctx': {'min_length': 1},
                       'url': 'https://errors.pydantic.dev/2.5/v/string_too_short'}]


def test_conversion_message():
    instance = ConversionMessage(**valid_conversion_message_dict)

    assert instance.identifier == '990b1738-80e6-45f6-9a89-a731cf4d27e5'

    assert is_conversion_type_mapping(instance.mapping, 'abc', 'xyz')

    assert isinstance(instance.data, ConversionData)
    assert instance.data.content_location == 'other/path/on/s3'

    assert len(instance.data.assets) == 3
    assert is_assert(instance.data.assets[0], "file_a.txt", "path/on/s3/1")
    assert is_assert(instance.data.assets[1], "file_b.txt", "path/on/s3/2")
    assert is_assert(instance.data.assets[2], "file_c.txt", "path/on/s3/3")

    assert len(instance.conversion_stack) == 3
    assert is_conversion_type_mapping(instance.conversion_stack[0], "abc", "123")
    assert is_conversion_type_mapping(instance.conversion_stack[1], "123", "456")
    assert is_conversion_type_mapping(instance.conversion_stack[2], "456", "xyz")

    assert isinstance(instance.meta, dict)
    assert instance.meta == {}


def test_conversion_message_with_empty_identifier():
    with pytest.raises(pydantic.ValidationError) as exc:
        conversion_message_dict = valid_conversion_message_dict.copy()
        conversion_message_dict['identifier'] = ""

        ConversionMessage(**conversion_message_dict)

    errors = exc.value.errors()

    assert errors == [{'type': 'string_too_short',
                       'loc': ('identifier',),
                       'msg': 'String should have at least 1 character',
                       'input': '',
                       'ctx': {'min_length': 1},
                       'url': 'https://errors.pydantic.dev/2.5/v/string_too_short'}]


def is_conversion_type_mapping(mapping: typing.Any, expected_from: str, expected_to: str) -> bool:
    assert isinstance(mapping, ConversionTypeMapping)
    assert mapping.from_content_type == expected_from
    assert mapping.to_content_type == expected_to

    assert mapping.model_dump(by_alias=True) == {"from": expected_from,
                                                 "to": expected_to}

    return True


def is_assert(asset: typing.Any, expected_filename: str, expected_content_location: str) -> bool:
    assert isinstance(asset, Asset)
    assert asset.filename == expected_filename
    assert asset.content_location == expected_content_location

    assert asset.model_dump(by_alias=True) == {"filename": expected_filename,
                                               "contentLocation": expected_content_location}

    return True
