from unittest import mock

import pika

from nldoc_p4_docx.nldoc_conversion_worker.config import S3Config, AmqpConnectionConfig
from nldoc_p4_docx.nldoc_conversion_worker.connection import establish_s3_connection, \
    establish_amqp_connection


@mock.patch("nldoc_p4_docx.nldoc_conversion_worker.connection.boto3")
def test_establish_s3(boto_mock):
    establish_s3_connection(
        S3Config(
            endpoint_url="http://localhost:9000",
            bucket="test",
            access_key="minioadmin",
            secret_key="minioadmin",
            region="us-east-1"
        )
    )

    boto_mock.client.assert_called_with(
        "s3",
        endpoint_url="http://localhost:9000",
        aws_access_key_id="minioadmin",
        aws_secret_access_key="minioadmin",
        region_name="us-east-1"
    )


@mock.patch("nldoc_p4_docx.nldoc_conversion_worker.connection.pika.BlockingConnection")
def test_establish_amqp(mock_pika_bc):
    establish_amqp_connection(
        AmqpConnectionConfig(
            host="localhost",
            port=5672,
            vhost="/",
            username="username",
            password="password",
        )
    )

    mock_pika_bc.assert_called_once_with(
        pika.ConnectionParameters(
            host="localhost",
            port=5672,
            virtual_host="/",
            credentials=pika.PlainCredentials(
                username='username',
                password='password',
            ),
        )
    )
