import boto3
import moto
import pytest

from nldoc_p4_docx.converter import DocxConverter
from nldoc_p4_docx.docx_processing.docx.document import Document
from nldoc_p4_docx.docx_processing.docx_document import DocxDocument
from nldoc_p4_docx.docx_processing.io import DocxIO
from nldoc_p4_docx.docx_processing.processor import DocxProcessor
from nldoc_p4_docx.nldoc_conversion_worker.dto import ConversionTypeMapping, ConversionData, \
    ConversionMessage
from nldoc_p4_docx.nldoc_conversion_worker.error import UnsupportedConversionException

supported_conversion = ConversionTypeMapping(
    **{"from": 'application/'
               'vnd.openxmlformats-officedocument.wordprocessingml.document',
       "to": 'application/'
             'vnd-nldoc.preprocessed+'
             'openxmlformats-officedocument.wordprocessingml.document'}
)

unsupported_conversion = ConversionTypeMapping(
    **{"from": 'application/pdf',
       "to": 'application/'
             'vnd-nldoc.preprocessed+'
             'openxmlformats-officedocument.wordprocessingml.document'}
)


def fake_docx_contents(text: str) -> str:
    return f"""
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<w:document
    xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
    xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
    xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
    xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
    xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
    xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
    xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
    xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
    xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
    xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
    xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
    xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:oel="http://schemas.microsoft.com/office/2019/extlst"
    xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
    xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
    xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
    xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
    xmlns:w10="urn:schemas-microsoft-com:office:word"
    xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
    xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
    xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
    xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
    xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
    xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
    xmlns:w16du="http://schemas.microsoft.com/office/word/2023/wordml/word16du"
    xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
    xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
    xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
    xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
    xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
    xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
    mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh wp14">
    <w:body>
        <w:p w:rsidR="00AA2C2E" w:rsidRDefault="00D63265" w:rsidP="00D63265">
            <w:r>
                <w:t>{{ text }}</w:t>
            </w:r>
        </w:p>
        <w:sectPr w:rsidR="00AA2C2E">
            <w:pgSz w:w="11906" w:h="16838"/>
            <w:pgMar
                w:top="1440"
                w:right="1440"
                w:bottom="1440"
                w:left="1440"
                w:header="708"
                w:footer="708"
                w:gutter="0"/>
            <w:cols w:space="708"/>
            <w:docGrid w:linePitch="360"/>
        </w:sectPr>
    </w:body>
</w:document>
            """


class FakeProcessor(DocxProcessor):
    def process(self, docx: DocxDocument) -> DocxDocument:
        docx.document = Document(fake_docx_contents("I am processed"))

        return docx


def create_message(mapping: ConversionTypeMapping) -> ConversionMessage:
    return ConversionMessage(
        identifier='test',
        mapping=mapping,
        data=ConversionData(contentLocation='test.docx', assets=[]),
        conversionStack=[mapping],
        meta={}
    )


def create_converter() -> DocxConverter:
    return DocxConverter(boto3.client('s3', region_name='us-east-1'), 'test-bucket')


def test_supported_conversions():
    assert create_converter().supported_conversions() == [supported_conversion]


def test_supports_conversion():
    converter = create_converter()

    assert converter.supports_conversion(supported_conversion)
    assert not converter.supports_conversion(unsupported_conversion)


def test_supports_message():
    converter = create_converter()

    assert converter.supports_message(create_message(supported_conversion))
    assert not converter.supports_message(create_message(unsupported_conversion))


@moto.mock_s3
@pytest.mark.parametrize('fixture_bytes', ['docx/docx/small.docx'], indirect=True)
def test_convert_supported_message(fixture_bytes: bytes):
    conn = boto3.resource("s3", region_name="us-east-1")
    conn.create_bucket(Bucket="test-bucket")

    s3 = boto3.client("s3", region_name="us-east-1")
    s3.put_object(Bucket="test-bucket", Key="test.docx", Body=fixture_bytes)

    message = create_message(supported_conversion)

    converter = create_converter()

    converted_message = converter.convert(message)

    assert converted_message.identifier == message.identifier
    assert converted_message.mapping == message.mapping
    assert converted_message.data.assets == message.data.assets
    assert converted_message.meta == message.meta

    assert converted_message.data.content_location != message.data.content_location
    assert converted_message.data.content_location.startswith('/test/p4.docx.')
    assert converted_message.conversion_stack == []

    s3.head_object(Bucket="test-bucket",
                   Key=converted_message.data.content_location)


def test_convert_unsupported_message():
    message = create_message(unsupported_conversion)

    converter = create_converter()

    with pytest.raises(UnsupportedConversionException):
        converter.convert(message)
