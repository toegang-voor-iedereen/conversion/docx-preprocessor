ARG BASE_IMAGE="registry.gitlab.com/toegang-voor-iedereen/docker-images/python/python:latest"

FROM ${BASE_IMAGE} AS base

WORKDIR /usr/src/app

COPY . .

RUN apk update && apk add --no-cache git libffi-dev openssl-dev build-base
RUN pip3 install .

FROM ${BASE_IMAGE}

COPY --from=base /usr/local/lib/python3.12/ /usr/local/lib/python3.12/
COPY --from=base /usr/local/bin/nldoc-p4-docx-worker /usr/local/bin/nldoc-p4-docx-worker

RUN adduser --system --no-create-home converter

USER converter

CMD ["nldoc-p4-docx-worker"]
