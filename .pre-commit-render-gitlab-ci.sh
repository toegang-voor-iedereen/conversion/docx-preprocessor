#!/bin/bash

jinja2 .gitlab-ci.j2.yml docker-recipe.yml --format=yaml -e jinja2_ext_path.dirname \
    | awk 'BEGIN{RS="";ORS="\n\n"}1' \
    | sed -e :a -e '/^\n*$/{$d;N;};/\n$/ba' > .gitlab-ci.yml
