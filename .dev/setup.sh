#!/bin/bash

set -euo pipefail

docker_build() {
    local dockerfile="$1"
    local tag="$2"

    shift 2

    docker build --no-cache -f "$dockerfile" -t "$tag" "$@" .
}

mkcert -install

mkcert "*.p4.local"

mkdir -p .dev/traefik/cert

mv _wildcard.p4.local*.pem .dev/traefik/cert
